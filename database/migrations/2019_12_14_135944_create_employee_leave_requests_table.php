<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leave_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('leave_type');
            $table->integer('leave_count');
            $table->string('reason');
            $table->string('start_date_session');
            $table->string('end_date_session');
            $table->integer('updated_by');
            $table->integer('employee_id');
            $table->integer('department_id');
            $table->integer('leave_status');
            $table->integer('old_leave_count');
            $table->timestamps();
            // leave_status ===> $table->integer('leave_status');
            // [0] - requested
            // [1] - approved_by_hod
            // [2] - rejected_by_hod
            // [3] - approved_by_principal
            // [4] - rejected_by_principal
            // [5] - completed
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leave_requests');
    }
}
