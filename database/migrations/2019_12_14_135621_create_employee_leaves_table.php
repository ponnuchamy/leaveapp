<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedDecimal('cl',4,2)->default(0.0);
            $table->unsignedDecimal('scl',4,2)->default(0.0);
            $table->unsignedDecimal('cpl',4,2)->default(0.0);
            $table->unsignedDecimal('el',4,2)->default(0.0);
            $table->unsignedDecimal('llp',4,2)->default(0.0);
            $table->integer('employee_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leaves');
    }
}
