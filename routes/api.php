<?php

use Illuminate\Http\Request;
use App\Helpers\ApiResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::get('reset', 'HomeController@resetPasword');
Route::post('verify-pasword', 'HomeController@verifyPasword');

Route::middleware('jwt.auth')->get('current_user', function () {
    $user =  auth('api')->user();
    $employee = $user->employee;
    $data = [
        "user" => $user,

    ];
    return ApiResponse::successResponse($data);

});

Route::post('register', 'HomeController@register');

Route::middleware('jwt.auth')->put('profile/update', 'HomeController@profile');
Route::middleware('jwt.auth')->post('change-password', 'HomeController@changePasword');


Route::middleware('jwt.auth')->get('employees', 'EmployeeController@index');
Route::middleware('jwt.auth')->get('posts/pagination', 'PostController@paginationPosts');
Route::middleware('jwt.auth')->get('employee/{id}', 'EmployeeController@show');
Route::middleware('jwt.auth')->post('employee', 'EmployeeController@store');
Route::middleware('jwt.auth')->post('employee/disable', 'EmployeeController@disablePost');
Route::middleware('jwt.auth')->put('employee/{employee}', 'EmployeeController@update');
Route::middleware('jwt.auth')->post('employee/delete', 'EmployeeController@delete');

// leave

Route::middleware('jwt.auth')->get('leave-list', 'EmployeeLeaveRequestController@index');
Route::middleware('jwt.auth')->post('leave-request', 'EmployeeLeaveRequestController@applyLeave');
Route::middleware('jwt.auth')->post('update/leave', 'EmployeeLeaveRequestController@updateLeave');
Route::middleware('jwt.auth')->post('approve/leave', 'EmployeeLeaveRequestController@approveLeave');