<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'id','first_name','last_name','employee_type','reference_number','department_id','user_id','phone','email','status'
    ];

    protected $casts = [
        'is_blocked' => "boolen"
    ];
    
    protected $dates = [
        "date_of_birth"=> "timestamp"
    ];

    public function leaves()
    {
        return $this->hasOne('App\Model\EmployeeLeave');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function department()
    {
        return $this->belongsTo('App\Model\Department');
    }

}
