<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class User extends Authenticatable  implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password','token','phone','email','role', ''
    ];

    protected $casts = [
        'disable_posts' => "array",
        'is_blocked' => "boolen"
    ];
    
    protected $dates = [
        "blocked_time"=> "timestamp"
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function generateToken($token)
    {
        $this->token = $token;
        $this->save();

        return $this->api_token;
    }
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    public function getJWTCustomClaims() {
        return [];
    }

    public function posts()
    {
        return $this->hasMany('App\Post')->orderBy('created_at','desc');
    }
    public function employee()
    {
        return $this->hasOne('App\Model\Employee');
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }
    public function user_likes()
    {
        return $this->belongsToMany('App\Post','likes','user_id','post_id');
    }
}
