<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeave extends Model
{
    protected $fillable = [
        'cl', 'scl', 'cpl','el','llp','employee_id'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Model\Employee','employee_id');
    }
}
