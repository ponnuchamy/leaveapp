<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeaveRequest extends Model
{
    
    protected $fillable = [
        'id','leave_type','leave_count','start_date_session','end_date_session','updated_by','employee_id','department_id','leave_status'
    ];

    // protected $casts = [
    //     'is_blocked' => "boolen"
    // ];
    
    protected $dates = [
        "start_date"=> "date",
        "end_date" => "date"
    ];

    public function department()
    {
        return $this->hasOne('App\Model\Department');
    }
    public function employee()
    {
        return $this->belongsTo('App\Model\Employee');
    }
}
