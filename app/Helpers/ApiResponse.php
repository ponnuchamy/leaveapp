<?php

namespace App\Helpers;
use Illuminate\Http\JsonResponse;

class ApiResponse {






	static public function errorResponse($status,$message)
	{
    	return response()->json([
    		'status' => $status,
    		'error' => $message
    	],422)->header('Access-Control-Allow-Origin' , '*')
                  ->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS');
	}

	static public function successResponse($data=array(),$message="success message")
    {
        return response()->json([
            'status' => 200,
            'message' => $message,
            'data' => $data
        ],200);
    }
    static public function paginationResponse($data=array(),$message="success message")
	{
    	return response()->json([
    		'status' => 200,
            'message' => $message,
    		'data' => [
                "current_page"  =>   $data['posts']['current_page'],
                "posts"         =>   $data['posts']['data'],
                "per_page"      =>   $data['posts']['per_page'],
                "total"         => $data['posts']['total']
            ]
    	],200);
	}
	
}
