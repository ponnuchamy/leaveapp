<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\ApiResponse;

use App\Model\Employee;
use App\Model\User;
use App\Model\EmployeeLeave;
use Auth;
use s3;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Filesystem\Filesystem;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::with('user')->get();
        return ApiResponse::successResponse($employees,'Employee list retrived successfully');
    }

    public function paginationPosts()
    {
        if(request('category_id'))
        {
            if(request('category_id')==0)
                $categoryId = [1,2,3,4,5];
            else
                $categoryId = [request('category_id')];
            if(Auth::user()->disable_posts)
            {

                $disabledUsers = Post::whereIn("id",Auth::user()->disable_posts)->pluck("user_id");

                $posts =  [
                    "posts" => Post::where("is_private",false)->whereIn('category_id',$categoryId)->whereNotIn("user_id",$disabledUsers)->with('user','comments.user')->orderBy('created_at','desc')->paginate(10)->toArray()
                ];
                
            }
            else
            {
                $posts =  [
                    "posts" => Post::where("is_private",false)->whereIn('category_id',$categoryId)->with('user','comments.user')->orderBy('created_at','desc')->paginate(10)->toArray()
                ];
                
            }
        }
        else
        {
            if(Auth::user()->disable_posts)
            {

                $disabledUsers = Post::whereIn("id",Auth::user()->disable_posts)->pluck("user_id");

                $posts =  [
                    "posts" => Post::where("is_private",false)->whereNotIn("user_id",$disabledUsers)->with('user','comments.user')->orderBy('created_at','desc')->paginate(10)->toArray()
                ];
                
            }
            else
            {
                $posts =  [
                    "posts" => Post::where("is_private",false)->with('user','comments.user')->orderBy('created_at','desc')->paginate(10)->toArray()
                ];
                
            }
            
        }
        
        return ApiResponse::paginationResponse($posts,'Post list retrived successfully');
    }

    public function show(Employee $employee)
    {
        return $employee;
    }

    public function leaves(Request $request)
    {
        $employee = Employee::find($request->id);
        $leaves = $employee->leaves()->get();
        $data = [
            "employee" => $employee,
            "leaves" => $leavescomments
        ];
        return ApiResponse::successResponse($data);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'first_name'      => 'required',
           'last_name'       => 'required|string',
           'reference_number'   => 'required',
           'employee_type'      => 'required',
           'gender'      => 'required',
           'department_id'      => 'required',
       ]);
        
       if ($validator->fails()) {
            $errors = [
                "errors" => $validator->errors()->all(),
                "message" => "Error Message"
            ];
            // Session::flash('error', $validator->messages()->first());
            return ApiResponse::errorResponse(422,$errors);
       }
       $user = new User;
        $user->user_name = $request->user["user_name"];
        $user->password = Hash::make($request->user["password"]);
        $user->email = $request->user["email"];
        $user->role = $request->user["role"];
       $user->save();
        
        $employee = new Employee;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->employee_type = $request->employee_type;
        $employee->gender = $request->gender;
        $employee->reference_number = $request->reference_number;
        $employee->department_id = $request->department_id;
        $employee->user_id = $user->id;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        $employee->status = "active";
        $employee->date_of_birth = $request->date_of_birth;
        $employee->save();
        $leaveTypes = new EmployeeLeave;
        $leaveTypes->cl = $request->leaves["cl"];
        $leaveTypes->scl = $request->leaves["scl"];
        $leaveTypes->cpl = $request->leaves["cpl"];
        $leaveTypes->el = $request->leaves["el"];
        $leaveTypes->llp = $request->leaves["llp"];
        $leaveTypes->employee_id = $employee->id;
        $leaveTypes->save();
        $employee->leaves;
        $employee->user;
        $data = [
            "employee" => $employee
        ];
        return ApiResponse::successResponse($data);
    }

    public function update(Request $request, Employee $employee)
    {
        $employee->update($request->all());
        $employee->leaves->update($request->leaves);
        $employee->leaves;
        $data = [
            "employee" => $employee,
        ];
        return ApiResponse::successResponse($data);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'employee_id'      => 'required|exists:employees,id',
       ]);
        
       if ($validator->fails()) {
            $errors = [
                "errors" => $validator->errors()->all(),
                "message" => "Invaid post"
            ];
            // Session::flash('error', $validator->messages()->first());
            return ApiResponse::errorResponse(422,$errors);
       }
       $user = Auth::user();
        $id = $request->employee_id;
        $employee = Employee::find($id);
        if($employee)
            $employee->delete();
        else
        {
            $errors = [
                "errors" => "Invalid Employee",
                "message" => "Error Message"
            ];
            // Session::flash('error', $validator->messages()->first());
            return ApiResponse::errorResponse(422,$errors);
        }

        return ApiResponse::successResponse($employee,204);
    }
         
}
