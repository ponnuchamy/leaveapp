<?php

namespace App\Http\Controllers;
use App\Model\User;
use App\Model\Employee;
use Hash;
use Mail;
use Auth;
use JWTAuth;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Illuminate\Support\Facades\Validator;
 use Notifiable;

 
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function profile(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'first_name'      => 'required',
        ]);
        if ($validator->fails()) {
            $errors = [
                "errors" => $validator->errors()->all(),
                "message" => "Error Message"
            ];
            // Session::flash('error', $validator->messages()->first());
            return ApiResponse::errorResponse(422,$errors);
       }
        // return $request->all();
        $employee = Auth::user()->employee;
        // return $employee;
        $employee->update($request->all());
        // $employee->leaves->update([$request->leaves]);
        $employee->leaves;
        $data = [
            "employee" => $employee,
        ];
        return ApiResponse::successResponse($data);
    }
    public function resetPasword(Request $request)
    {
        $token =  mt_rand(100000, 999999);
        $user = User::where("email",$request->email)->first();

        Mail::send('reset.emailer', ['user' => $user,'token'=> $token], function ($m) use ($user) {
            $m->from('hello@app.com', 'PSNA');

            $m->to($user->email, $user->name)->subject('Your Pin');
        });
        $user->generateToken($token);
        
        return ApiResponse::successResponse($user);
    }
    public function verifyPasword(Request $request)
    {
        $token =  $request->token;
        $user = User::where("email",$request->email)->first();
        if ($user && $user->token == $token)
        {
            $user->update(["token"=>""]);
            $user->update(["password"=>Hash::make($request->password)]);
            $token =  JWTAuth::fromUser($user);
            $data = [
                "user" => $user,
                "token" => $token,
            ];
            return ApiResponse::successResponse($data);
        }
        else
        {
            $errors = [
                "errors" => "Invalid Token or user name",
                "message" => "Error Message"
            ];
            return ApiResponse::errorResponse(422,$errors);
        }
        
        
    }
    public function changePasword(Request $request)
    {
        $user = auth('api')->user();
        if ($user)
        {
            $user->update(["password"=>$request->password]);
            return ApiResponse::successResponse($user);
        }
        else
        {
            $errors = [
                "errors" => "Invalid Token or user name",
                "message" => "Error Message"
            ];
            return ApiResponse::errorResponse(422,$errors);
        }
        
        
    }

    public function register(Request $request)
    {
        // Here the request is validated. The validator method is located
        // inside the RegisterController, and makes sure the name, email
        // password and password_confirmation fields are required.
        // $this->validator($request->all())->validate();
         $validator = Validator::make($request->all(), [
           'email' => 'required|email|unique:users',
           'user_name' => 'required|string|max:50',
           'password' => 'required|min:6|confirmed'
       ]);
        
       if ($validator->fails()) {
            $errors = [
                "errors" => $validator->errors()->all(),
                "message" => "Error Message"
            ];
            return ApiResponse::errorResponse(422,$errors);
       }
        $user =  User::create([
            'user_name' => $request['user_name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $user = User::where("email",$request['email'])->first();
        $token =  mt_rand(100000, 999999);
        $user->generateToken($token);
      
        return ApiResponse::successResponse($user,'user registration successfully completed');
        
    }


}