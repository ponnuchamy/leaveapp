<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Helpers\ApiResponse;
use \Carbon\carbon;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login() {
        $credentials = request(['email', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return ApiResponse::errorResponse(422,['message' => 'Please check your credentials']);
        }
        $user = auth('api')->user();
        $timeNow = Carbon::parse(date('Y-m-d H:i:s'));
        if($user->block_count >= 5)
        {
            if($timeNow->diffInhours($user->blocked_time) >= 23)
            {
                $user->block_count = 0;
                $user->blocked_time = null;
                $user->save();
            }
            else
            {
                return ApiResponse::errorResponse(422,['message' => 'Your Account has been blocked']);
            }
        }
        $data = [
            'token' => $token,
            'user' => auth('api')->user(),
            'expires' => auth('api')->factory()->getTTL() * 60,
        ];
        return ApiResponse::successResponse($data);

    }
}
