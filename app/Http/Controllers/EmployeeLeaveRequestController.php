<?php

namespace App\Http\Controllers;
use App\Model\User;
use App\Model\Employee;
use App\Model\EmployeeLeaveRequest;
use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Illuminate\Support\Facades\Validator;
use Notifiable;

 
class EmployeeLeaveRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaveList = EmployeeLeaveRequest::with('employee')->get();
        $data = [
            "leave_list" => $leaveList
        ];
        return ApiResponse::successResponse($data);
    }
    public function applyLeave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'leave_type'      => 'required',
            'start_date'   => 'required',
            'start_date_session'   => 'required',
            'end_date'      => 'required',
            'end_date_session'      => 'required',
            'reason'      => 'required',
            'department_id'      => 'required',
        ]);
         
        if ($validator->fails()) {
             $errors = [
                 "errors" => $validator->errors()->all(),
                 "message" => "Error Message"
             ];
             // Session::flash('error', $validator->messages()->first());
             return ApiResponse::errorResponse(422,$errors);
        }
        $leave = new EmployeeLeaveRequest;
        $leave->leave_type = $request->leave_type;
        $leave->leave_count = 1;
        $leave->start_date_session = $request->start_date_session;
        $leave->start_date = $request->start_date;
        $leave->end_date_session = $request->end_date_session;
        $leave->end_date = $request->end_date;
        $leave->updated_by = Auth::user()->id;
        $leave->employee_id = Auth::user()->employee->id;
        $leave->department_id = $request->department_id;
        $leave->reason = $request->reason;
        $leave->leave_status = 0;
        $leave->save();


        
        $data = [
            "leave" => $leave,
        ];
        return ApiResponse::successResponse($data);
    }
    public function updateLeave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'leave_id'      => 'required|exists:employee_leave_requests,id',
            'leave_type'      => 'required',
            'start_date'   => 'required',
            'start_date_session'   => 'required',
            'end_date'      => 'required',
            'end_date_session'      => 'required',
            'reason'      => 'required',
            'department_id'      => 'required',
        ]);
         
        if ($validator->fails()) {
             $errors = [
                 "errors" => $validator->errors()->all(),
                 "message" => "Error Message"
             ];
             // Session::flash('error', $validator->messages()->first());
             return ApiResponse::errorResponse(422,$errors);
        }
        $leave = EmployeeLeaveRequest::find($request->leave_id);
        $leave->leave_type = $request->leave_type;
        $leave->leave_count = 1;
        $leave->start_date_session = $request->start_date_session;
        $leave->start_date = $request->start_date;
        $leave->end_date_session = $request->end_date_session;
        $leave->end_date = $request->end_date;
        $leave->updated_by = Auth::user()->id;
        $leave->reason = $request->reason;
        $leave->leave_status = 0;
        $leave->save();


        
        $data = [
            "leave" => $leave,
        ];
        return ApiResponse::successResponse($data);
    }
    public function approveLeave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'leave_id'      => 'required|exists:employee_leave_requests,id',
            'status'      => 'required',
        ]);
         
        if ($validator->fails()) {
             $errors = [
                 "errors" => $validator->errors()->all(),
                 "message" => "Error Message"
             ];
             // Session::flash('error', $validator->messages()->first());
             return ApiResponse::errorResponse(422,$errors);
        }
        $leave = EmployeeLeaveRequest::find($request->leave_id);
        $leave->leave_status = $request->status;
        $leave->save();

        $data = [
            "leave" => $leave,
        ];
        return ApiResponse::successResponse($data);
    }


    


}